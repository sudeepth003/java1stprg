
public class ObjectTypeConvversion {

	public static void main(String[] args) {
		
		//converting a primitive to Object type
		
		int number= 100;
		int number2 = 1002;
		Integer obj = Integer.valueOf(number);//converting int
		Integer objNum = number2;// implicittype  cov -> autoboxing
		System.out.println(number + "   "+ obj);
		System.out.println(number2+"  "+ objNum);
		
		
		//converting obj type to primitive type
		
		Integer obj2 = new Integer (100);
		Integer intObj2 = new Integer (500);
		int num = obj2.intValue();// converting obj int to primitive int explicity
		int num2 = intObj2;//explicity -> unboxing
		System.out.println(num + "  " + obj2);
		
		
		
		
		
		//
		float f1 = 100;
		Float f2 = Float.valueOf(f1);
		System.out.println(f1+"   "+f2);
		//

		Float f3 = new Float (100);
		float f4 = f3.intValue();
		System.out.println(f3 + "  " + f4);	
		
		
		

	}

}

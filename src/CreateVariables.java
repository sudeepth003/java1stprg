
public class CreateVariables {
	
	public static void main(String[] args) {
		
		int i=30;
		float f= 20.0f;
		double d=10.0d;
		long l=300l;
		char c='s';
		
		System.out.println("Integer value of i is "+i+" Take 4 byte");
		System.out.println("Float value of f is "+f+" Take 4 byte");
		System.out.println("Double value of d is"+d+" Take 8 byte");
		System.out.println("Long value of L is "+l+" Take 8 byte");
		System.out.println("Char value of C is "+c+" Take 2 byte");
		
	} 

}


public class UnaryOperator {

	public static void main(String[] args) {
		
		int a = 20;
		System.out.println(a);	//20
		System.out.println(a++);//20
		System.out.println(++a);//22
		System.out.println(a);
		
		
		int c = a;
		System.out.println(c++);//22
		System.out.println(--c);//22
		System.out.println(++c);//23
		System.out.println(c++);//23
		System.out.println(c--);//24
		System.out.println(c);
		
		float z = c;
		
		System.out.println(z);//23.0
		z = z+10+4;
		System.out.println(z);//37.0
		System.out.println(z+1);//38.0
		System.out.println(z++);//38.0
		System.out.println(--z);//37.0
		
		//short notation
		
		int w = (int)(z+z);
		
		System.out.println(w);//74
		System.out.println(z+=z);//74.0
		z=z+z;
		System.out.println(z);//148.0
		z -=z;
		System.out.println(z);//0.0
		z+=1;
		System.out.println(z);//1.0
		
		
		
		
		
		
	}

}


public class Turnary {

	public static void main(String[] args) {
		
		int age = 10;
		int z = 0;
		//if (age >20) {
			
			
		//	z = age;
						
	//	}else{
			
			//z=0;
			
		//}
		
		//turnary operator
		//variable x = (expression) ? value if true: value if false
		//z = (age>20) ? age : 1;
		z = (age>20) ? age : (age>18)? 18 : 0;
		System.out.println(z);
		
		String loginMsg = (z!=0) ? "login success" : "login Failed ";
		
		System.out.println(loginMsg);
		
	}

}

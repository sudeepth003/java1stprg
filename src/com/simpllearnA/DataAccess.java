package com.simpllearnA;

public class DataAccess {
	
	// private is Accessible within a class.
	
	private static int privateV;
	// default is accessible within a package.
	int defaultV;
	
	//protected is accessible within a class , package 
	
	// outside the package but with inheritance.
	protected static int protectedV;
	// public is accessible anywhere
	public int pulicV;
	
	//inner class
	private class PrivateClass{}
	class DefaultClass{}
	protected static class ProtectedClass{}
	public class PublicClass{}
	
	
	//method
	
	private void privateMethod( ) {};
	
	void defaultMethod () {};
	
	protected int protectedMethod() { return 10;};
	
	public void publicMethod () {};
	

	public static void main(String[] args) {
		
		 DataAccess d = new DataAccess ();
		  
		 
		 
		 
		 
		 
		System.out.println("data"+privateV);
		

	}

}

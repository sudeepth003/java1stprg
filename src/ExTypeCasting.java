
public class ExTypeCasting {

	public static void main(String[] args) {
		
		byte b = 10 ;
		int i = 20 ;
		double d = 1000.0d;
		long l = (long) d ;
		int i1 = (int)l;
		double dl = d + l;
		long l1 = l + (long)dl ;
		int i2 = b + i + (int)d + (int)l + i1 + (int) dl;		
		
		System.out.println(l);
		System.out.println(i1);
		System.out.println(dl);
		System.out.println(l1);
		System.out.println(i2);
		

	}

}
